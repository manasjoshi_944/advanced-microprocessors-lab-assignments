org 0x8000
bits 16
	push  0xb800
	pop   es     		; Set ES to the Video Memory
	;; Clear screen
	mov ax, 0x0010    	; Clean screen green background
	call  cls
	;; Print message
	call  print
	;; Done!
	jmp $   ; this freezes the system, best for testing
	hlt	;this makes a real system halt
	ret     ;this makes qemu halt, to ensure everything works we add both

cls:
	xor   di,di
	mov   cx, 80*24		;Default console size
	repnz stosw
	ret

print:
	xor   di, di
	add   di, 808
.loop:
	mov ah, 10H
	int 16H
	mov   ah, 0x2f
        stosw	    ; Stores AX (char + color)
	jmp   .loop ;print next character

.end:
	ret


times 512-($-$$) db 0 ; Make it a disk sector

